module.exports = function(config) {
  config.set({
   // logLevel: "trace",
    mutator: "javascript",
    packageManager: "npm",
    reporters: ["html", "clear-text", "progress"],
    testRunner: "karma",
    transpilers: [],
    testFramework: "jasmine",
    coverageAnalysis: "perTest",
    karma: {
      projectType: "custom",
      configFile: "karma.conf.js",
    },
    mutate: ["js/**/*.js"]
  });
};
